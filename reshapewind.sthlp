
{smcl}
{* 26 Dec 2021}{...}
{hline}
help for {hi:reshapewind}
{hline}

{title:Title}

{p 4 4 2}
{bf:reshapewind} —— Convert the data downloaded from the {bf:Wind} database or {bf:Choice} database into long panel data

{title:Syntax}

{p 4 4 2}
{cmdab:reshapewind} 
{cmdab:TimeType}
[{cmdab:Speed}]

{title:Description}

{p 4 4 2}
{cmd:reshapewind} It can help you solve the problem of data conversion downloaded from Wind or Choice {bf:simply} and efficiently

{title:Options}

{p 4 4 2}  
{bf:TimeType}： Frequency of data to be converted. For annual data, enter {bf:year} or {bf:y}. For quarterly data, please enter {bf:quarter} or {bf:q}.{p_end} 
{p 4 4 2}  
{bf:Speed}：Optional, enter {bf:speed} or {bf:s} to accelerate the conversion. The acceleration condition is: {bf: the data types of all time points under the same variable must be consistent} {p_end}

{title:Examples}

{p 4 4 2} import excel using 全部A股.xlsx,clear first

{p 4 4 2} *- {bf:reshape annual!}. {p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind year"'}{p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind y"'}{p_end}
Choose one of the above two commands

{p 4 4 2} *- {bf:reshape quarter!}. {p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind quarter"'}{p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind q"'}{p_end}
Choose one of the above two commands

{p 4 4 2} *- {bf:Speed!}. {p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind y s"'}{p_end}
{p 4 4 2}{inp:.} {stata `"reshapewind q s"'}{p_end}

{title:Author}

{p 4 4 2}{cmd:EmperorHeart}{p_end}
{p 4 4 2} .Finance School,Jiangxi University of Finance and Economics,China.{p_end}
{p 4 4 2} .E-mail: 1790911744@qq.com{p_end}


