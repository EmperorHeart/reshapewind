/*
作者：EmperorHeart

版本：1.6

更新时间：2023年3月12日
*/
capture program drop reshapewind
program reshapewind
version 15.0
	args TimeType Speed Other
	* 检验语法
	if "`TimeType'" != "year" & "`TimeType'" != "y" & "`TimeType'" != "quarter" & "`TimeType'" != "q"{
		di as error "Please input a valid TimeType ! year(y) or quarter(q) ?"
		exit
	}
	
	if "`Speed'"!="" & "`Speed'"!="s" & "`Speed'"!="speed"{
		di as error "please input a valid Speed sign!"
		exit
	} 
	if "`Other'" != ""{
		di as err "You have entered too many parameters !"
		exit
	}
	
	
	*检验数据基本格式
	qui ds 
	local varlistAll=r(varlist)
	isin "证券简称" "`varlistAll'"
	local m1=r(bool)
	isin "证券名称" "`varlistAll'"
	local m2=r(bool)
	isin "证券代码" "`varlistAll'"
	local m3=r(bool)
	if !((`m1'==1 & `m3'==1 ) | (`m2'==1 & `m3'==1 )) {
		di as error `"No "证券简称" and "证券代码" Or "证券名称" and "证券代码" in your data ! "'
		exit
	}

	*格式正确，语法正确，执行下面操作
	cap drop if 证券简称=="" 
	cap drop if 证券名称==""

	order _all,alpha    
	cap order 证券代码 证券简称
	cap order 证券代码 证券名称
	
	if "`Speed'"==""{
		qui tostring *,force replace  
	} 
	
	if "`TimeType'"=="year" | "`TimeType'"=="y" {
		local qians=""
		foreach var of varlist _all{
			if regexm("`var'","([0-9][0-9][0-9][0-9])"){
						local z=regexs(0)
						 }			 
			local pos=strpos("`var'","`z'")        //年份位置
			local qian=substr("`var'",1,`pos'-1)  //变量前缀	 
			local newname="`qian'"+"`z'" 
			local qians="`qians' "+"`qian'"   //变量前缀集合
			cap rename `var' `newname'     //改名
		}

		unique "`qians'"
		local unions = r(varList)
		
		qui ds
		tokenize `r(varlist)'
		if "`2'"=="证券简称"{
			 cap fastreshape long `unions',i(证券代码 证券简称) j(year) fast
             ** 若fastreshape无法转化则交给reshape解决
             if (_rc != 0){
                qui reshape long `unions',i(证券代码 证券简称) j(year)
             }
		}
		if "`2'"=="证券名称"{
			 cap fastreshape long `unions',i(证券代码 证券名称) j(year) fast
             if (_rc != 0){
                qui reshape long `unions',i(证券代码 证券名称) j(year)
             }
		}
        ** 因发现年度转化后可能没有排序，因此需要加入下这条命令
			 qui sort 证券代码 year   
	}
	  
	if "`TimeType'" == "quarter" | "`TimeType'" == "q"{
		local qians=""
		foreach var of varlist _all{
			if regexm("`var'","([0-9]+)"){
						local z=regexs(0)
						 }
			if strlen("`z'")>4{
					 local z=substr("`z'",1,6)
					 local q=""
					 }
			local pos=strpos("`var'","`z'")        //年份位置
			local qian=substr("`var'",1,`pos'-1)  //变量前缀
			  if regexm("`var'", "一季"){
					local q="03"
							 }
			  if regexm("`var'", "中报") | regexm("`var'", "二季"){
				   local q="06" 
							 }
			  if regexm("`var'", "三季"){
					   local q="09" 
							 }	 
			  if regexm("`var'", "年报"){
						local q="12" 
							 }
			local newname="`qian'"+"`z'"+"`q'"
			local qians="`qians' "+"`qian'"   //变量前缀集合
			cap rename `var' `newname'     //改名
		}
		
		unique "`qians'"
		local unions = r(varList)
		
		qui ds
		tokenize `r(varlist)'
		if "`2'"=="证券简称"{
			 cap fastreshape long `unions',i(证券代码 证券简称) j(_quarter) fast
             if (_rc != 0){
                qui reshape long `unions',i(证券代码 证券简称) j(_quarter)
             }
		}
		if "`2'"=="证券名称"{
			 cap fastreshape long `unions',i(证券代码 证券名称) j(_quarter) fast
             if (_rc != 0){
                qui reshape long `unions',i(证券代码 证券名称) j(_quarter)
             }
		}

		 **扫尾工作
		qui{
			tostring _quarter,force replace
			g year=ustrleft(_quarter,4)
			destring year,force replace
			g _q=ustrright(_quarter,2)
			destring _q,force replace
			g q=_q/3
			g quarter=yq(year,q)
			format quarter %tq
			drop _quarter _q
			cap sort 证券代码 证券简称 year q
			cap sort 证券代码 证券名称 year q

			cap order 证券代码 证券简称 year q quarter
			cap order 证券代码 证券名称 year q quarter
		}
	}
    
	*去除一些无关紧要的词
	foreach var in 报告期 交易日期 百万元 万元 千元 元{
		qui renvarlab,subst("`var'")
	}

	** 解决最后一公里的问题:如何自动识别哪些数据是数值型、字符型？。
	if "`Speed'"==""{
		snapshot erase _all
		snapshot save

		local numList=""
		qui{
			foreach v of varlist _all{
				destring `v',force replace
				su `v'
				if (r(N)>0){
					local numList = "`numList'"+" "+"`v'"
				}
			}
			local numList=strtrim("`numList'")
			snapshot restore 1
			destring `numList',force replace
		}	
	}
end

capture program drop unique
program unique,rclass
version 15.0
	local qians=strtrim("`1'")   
	local _pos=strpos("`qians'"," ")    
	local unions=substr("`qians'",1,`_pos'-1)  //截取第一个名字
	local z="`unions'"              
	foreach qian of local qians{
		if "`qian'"=="`z'"{
			continue
			 }
		local z="`qian'"          //去重中间变量
		local unions="`unions' "+"`z'"
	}
	return local varList="`unions'"
end

capture program drop isin
program isin,rclass
version 15.0
	local m=0
	foreach var in `2'{
		if ("`var'"=="`1'"){
			local m=1
			continue,break
		}
	}
	return local bool=`m'
end